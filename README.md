# quiz_engine_creator

Ένα πολύ απλό python 3.0 script, το οποίο παίρνει ως είσοδο ένα αρχείο υπολογιστικού φύλλου (spreadsheet) του LibreOffice (LibreOffice Impress) και δίνει ως έξοδο ένα json αρχείο κουίζ συμβατό με την εφαρμογή [quiz_engine](https://github.com/ale3andro/quiz_engine)

Το αρχείο υπόδειγμα του LibreOffice Impress, συμπεριλαμβάνεται σε αυτό το αποθετήριο με όνομα sample.ods

# Χρήση εφαρμογής

python3 quizengine_creator.py

Στο παράθυρο επιλογής αρχείου που ανοίγει, επιλέγουμε το ods αρχείο που περιέχει τις ερωτήσεις του κουίζ



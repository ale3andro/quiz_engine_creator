try:
    from pandas_ods_reader import read_ods
except ImportError:
    print('pandas_ods_reader module is not installed')
    exit(1)
import json, easygui, os, sys

script_directory = os.path.dirname(os.path.abspath(sys.argv[0]))

workfile=easygui.fileopenbox(msg="Διάλεξε ods αρχείο", default=script_directory + "/*.ods")
sheet = "Φύλλο1"
df = read_ods(workfile , sheet)
#print(df)
quiz = []

header = { "id": df['id'][0],
            "class": df['class'][0],
            "description" : df['description'][0],
            "shuffle_questions" : df['shuffle_questions'][0],
            "send_score" : df['send_score'][0],
            "allow_reset" : df['allow_reset'][0],
            "shuffle_answers" : df['shuffle_answers'][0]
        }
quiz.append(header)

for i in range(2, len(df['id'])):
    question = { "question" : df['id'][i],
                    "image" : df['class'][i],
                    "option0" : df['description'][i],
                    "option1" : df['shuffle_questions'][i],
                    "option2" : df['send_score'][i] if df['send_score'][i] is not None else '',
                    "option3" : df['allow_reset'][i] if df['allow_reset'][i] is not None else '',
                    "correct" : df['shuffle_answers'][i],
                    "hint" : df['hint'][i]
                }
    quiz.append(question)

#print(quiz)
json_object = json.dumps(quiz, indent=4)
 
# Writing to sample.json
with open(df['id'][0] + ".json", "w") as outfile:
    outfile.write(json_object)